# Lambdanarium

## My personal website

### URL

> <https://lambdaclan.gitlab.io>

### Technology stack

- Static site generator
  - [Hugo](https://gohugo.io/documentation/)
- Theme
  - [PaperMod](https://github.com/adityatelange/hugo-PaperMod/wiki)
- Formatters
  - [Prettier](https://prettier.io/docs/en/)
  - [xmllint](https://gnome.pages.gitlab.gnome.org/libxml2/xmllint.html)
- Linters
  - [markdownlint](https://github.com/igorshubovych/markdownlint-cli?tab=readme-ov-file#usage)
  - [xmllint](https://gnome.pages.gitlab.gnome.org/libxml2)
- [yamllint](https://yamllint.readthedocs.io/en/stable/)
- Utils
  - [fd](https://github.com/sharkdp/fd?tab=readme-ov-file#how-to-use)
  - [just](https://github.com/casey/just?tab=readme-ov-file#quick-start)

### Automation

- Format markdown files

  > just format-md

- gfg
