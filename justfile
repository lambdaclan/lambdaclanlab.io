serve:
    #!/usr/bin/env bash
    set -euo pipefail
    hugo server

check-md:
    #!/usr/bin/env bash
    set -euo pipefail
    fd . --extension md content/ --exec prettier --check --parser markdown {}
    prettier --check --parser markdown README.md
    fd . --extension md content/ --exec markdownlint {}
    markdownlint README.md

fix-md:
    #!/usr/bin/env bash
    set -euo pipefail
    fd . --extension md content/ --exec prettier --write --parser markdown {}
    prettier --write --parser markdown README.md
    fd . --extension md content/ --exec markdownlint --fix {}
    markdownlint --fix README.md

check-html:
    #!/usr/bin/env bash
    set -euo pipefail
    fd . --extension html layouts/ --exec prettier --check --parser html {}

fix-html:
    #!/usr/bin/env bash
    set -euo pipefail
    fd . --extension html layouts/ --exec prettier --write --parser html {}

check-xml:
    #!/usr/bin/env bash
    set -euo pipefail
    fd . --extension xml static/ --exec xmllint --noout {}

fix-xml:
    #!/usr/bin/env bash
    set -euo pipefail
    fd . --extension xml static/ --exec xmllint --format {} --output {}

check-yaml:
    #!/usr/bin/env bash
    set -euo pipefail
    prettier --check --parser yaml hugo.yaml
    prettier --check --parser yaml .gitlab-ci.yml
    yamllint hugo.yaml
    yamllint .gitlab-ci.yml

fix-yaml:
    #!/usr/bin/env bash
    set -euo pipefail
    prettier --write --parser yaml hugo.yaml
    prettier --write --parser yaml .gitlab-ci.yml

check-just:
    #!/usr/bin/env bash
    set -euo pipefail
    just --fmt --check --unstable
    just --list | tail --lines +2 | awk '{$1=$1};1' | xargs --replace bash -c "just --show {} | tail --lines +2 | cut --characters=5- | shellcheck --exclude=SC2016 -"
    just --list | tail --lines +2 | awk '{$1=$1};1' | xargs --replace bash -c "just --show {} | tail --lines +2 | cut --characters=5- | shfmt --indent 2 --diff -"

fix-just:
    #!/usr/bin/env bash
    set -euo pipefail
    just --fmt --unstable --justfile justfile

check: check-md check-html check-xml check-yaml check-just
    #!/usr/bin/env bash
    set -euo pipefail
    :

update-theme:
    #!/usr/bin/env bash
    set -euo pipefail
    git submodule update --remote --merge
